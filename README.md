# LLM Bootstrap for Gravity Forms

## Description



<strong>This plugin requires PHP 5.6+.</strong>

## Installation
1. Download and activate the plugin through the \'Plugins\' menu in WordPress.
2. Click the \'Settings\' link on the Plugins page to start the process.

## Frequently Asked Questions
### What versions of Bootstrap do you support?

### Will a new update invalidate my styling?
No! We will add new setting options for every minor update of Bootstrap, including new Alpha/Beta releases.

### Will you ever support Bootstrap V.3.X.X?
Maybe. Let us know which version you are using and we will work to setup something for you.