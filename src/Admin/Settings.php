<?php

namespace LlmBootstrapForGravityForms\Admin;

/**
 * Settings
 *
 * Plugin settings and setting pages.
 * @package LlmBootstrapForGravityForms
 */

use LlmHubspotBlogImport\BackgroundSave;
use LlmHubspotBlogImport\Save;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Settings {
	public function __construct() {
		$this->hooks();
	}

	public function hooks() {
		add_action( 'admin_menu', [ $this, 'menuOptions' ] );
	}

	public function customEnqueue() {
		//
	}

	/**
	 * Create the menu options for Settings
	 */
	public function menuOptions() {
		// This page will be under "Settings"
		add_options_page( 'LLM Bootstrap for Gravity Forms', 'Bootstrap for Gravity Forms', 'manage_options',
			'llm-bootstrap-for-gravity-forms-settings', [ $this, 'createAdminPage' ] );
	}

	/**
	 * Admin Page Specific Code
	 */
	public function createAdminPage() {
		$bsversion = get_option( 'llm_bootstrap_for_gravity_forms_bs_version' );
		?>

		<div class="wrap">
			<h2>LLM Bootstrap for Gravity Forms Settings</h2>

			<form method="post" action="options.php">
				<?php settings_fields( 'llm-bootstrap-for-gravity-forms-settings' ); ?>
				<?php do_settings_sections( 'llm-bootstrap-for-gravity-forms-settings' ); ?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">Select Your Bootstrap Version</th>
						<td>
							<select name="llm_bootstrap_for_gravity_forms_bs_version">
								<option value="v4 - Alpha 6" <?php if ($bsversion == 'v4 - Alpha 6' ) echo 'selected="selected"'; ?>>v4 - Alpha 6</option>
								<option value="v3.3.7" <?php if ($bsversion == 'v3.3.7' ) echo 'selected="selected"'; ?>>v3.3.7</option>
							</select>
						</td>
					</tr>
				</table>
				<?php submit_button(); ?>
			</form>
		</div>

		<?php
	}
}

/**
 * Run the class at start
 */
return new Settings();