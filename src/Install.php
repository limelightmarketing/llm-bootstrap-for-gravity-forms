<?php

namespace LlmBootstrapForGravityForms;

/**
 * Install
 *
 * Installation related functions and actions.
 * @package LlmBootstrapForGravityForms
 */

if ( ! defined('ABSPATH')) {
    exit;
}

class Install
{
    public function __construct()
    {
        // Remove this when we are ready to have a full install/uninstall process.
        $this->hooks();
        $this->mkdir();
        $this->addSettingOptions();
    }

    public function install()
    {
        self::mkdir();
        self::addSettingOptions();
    }

    public function hooks()
    {
        add_action( 'admin_init', [$this, 'addSettingOptions']);
    }

    public function mkdir()
    {
        //
    }

    public function addSettingOptions()
    {
        // Register our settings
        register_setting('llm-bootstrap-for-gravity-forms-settings', 'llm_bootstrap_for_gravity_forms_bs_version');
    }
}

return new Install();