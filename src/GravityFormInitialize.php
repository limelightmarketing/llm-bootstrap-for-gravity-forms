<?php

namespace LlmBootstrapForGravityForms;

class GravityFormInitialize {
	public function __construct() {
		$this->hooks();
		$this->includes();
	}

	public function hooks() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueScriptsStyles' ] );
		add_filter( 'gform_field_container', [ $this, 'addFieldContainerClasses' ], 10, 6 );
		add_filter( 'gform_field_content', [ $this, 'addFieldInputClass' ], 10, 5 );
		add_filter( 'gform_submit_button', [ $this, 'addBtnPrimaryClass' ], 20, 3 );
	}

	public function includes() {
		//
	}

	/**
	 * Load styling based on the Bootstrap Version selected
	 */
	public function enqueueScriptsStyles() {
		if ( ! empty( get_option( 'llm_bootstrap_for_gravity_forms_bs_version' ) ) ) {
			if ( get_option( 'llm_bootstrap_for_gravity_forms_bs_version' ) == 'v3.3.7' ) {
				wp_enqueue_style( 'gravity-forms-bootstrap-3-3-7',
					\LlmBootstrapForGravityForms::instance()->pluginUrl() . '/assets/bootstrap-3-3-7.css' );
			} elseif ( get_option( 'llm_bootstrap_for_gravity_forms_bs_version' ) == 'v4 - Alpha 6' ) {
				wp_enqueue_style( 'gravity-forms-bootstrap-4-alpha-6',
					\LlmBootstrapForGravityForms::instance()->pluginUrl() . '/assets/bootstrap-4-alpha-6.css' );
			}
		}
	}

	/**
	 * Add Bootstrap field classes to Gravity Forms
	 */
	public function addFieldContainerClasses( $field_container, $field, $form, $css_class, $style, $field_content ) {
		$id       = $field->id;
		$field_id = is_admin() || empty( $form ) ? "field_{$id}" : 'field_' . $form['id'] . "_$id";

		return '<li id="' . $field_id . '" class="' . $css_class . ' form-group">{FIELD_CONTENT}</li>';
	}

	public function addFieldInputClass( $content, $field, $value, $lead_id, $form_id ) {
		if ( $field["type"] != 'hidden' && $field["type"] != 'list' && $field["type"] != 'multiselect' && $field["type"] != 'checkbox' && $field["type"] != 'fileupload' && $field["type"] != 'date' && $field["type"] != 'html' && $field["type"] != 'address' ) {
			$content = str_replace( 'class=\'medium', 'class=\'form-control medium', $content );
		}

		if ( $field["type"] == 'name' || $field["type"] == 'address' ) {
			$content = str_replace( '<input ', '<input class=\'form-control\' ', $content );
		}

		if ( $field["type"] == 'textarea' ) {
			$content = str_replace( 'class=\'textarea', 'class=\'form-control textarea', $content );
		}

		if ( $field["type"] == 'checkbox' ) {
			$content = str_replace( 'li class=\'', 'li class=\'checkbox ', $content );
			$content = str_replace( '<input ', '<input style=\'margin-left:1px;\' ', $content );
		}

		if ( $field["type"] == 'radio' ) {
			$content = str_replace( 'li class=\'', 'li class=\'radio ', $content );
			$content = str_replace( '<input ', '<input style=\'margin-left:1px;\' ', $content );
		}

		return $content;
	}

	function addBtnPrimaryClass( $button, $form ) {
		return "<input type='submit' class='btn btn-primary' id='gform_submit_button_{$form['id']}' value='Submit' tabindex='2' onclick='if(window[&quot;gf_submitting_1&quot;]){return false;}  window[&quot;gf_submitting_1&quot;]=true;  ' onkeypress='if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} window[&quot;gf_submitting_1&quot;]=true;  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }'>";
	}
}

return new GravityFormInitialize();