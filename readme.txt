=== LLM Bootstrap for Gravity Forms ===
Contributors: failcookie, limelightmarketing
Tags: bootstrap, gravity forms, bootstrap styling, bootstrap 4
Requires at least: 4.4
Tested up to: 4.7.3
Stable tag: trunk
License: GPL2

Add Bootstrap styling support to Gravity Forms at ease. Select the version that you are using across your site, including Bootstrap 3 and the newest Bootstrap 4.

== Description ==

<strong>This plugin requires PHP 5.6+.</strong>

== Installation ==
1. Download and activate the plugin through the \'Plugins\' menu in WordPress.
2. Click the \'Settings\' link on the Plugins page to start the process.

== Frequently Asked Questions ==
= What versions of Bootstrap do you support? =

= Will a new update invalidate my styling? =
No! We will add new setting options for every minor update of Bootstrap, including new Alpha/Beta releases.

= Will you ever support Bootstrap V.3.X.X? =
Maybe. Let us know which version you are using and we will work to setup something for you.

== Screenshots ==

== Changelog ==
1.0 - Initial release.